from setuptools import setup, find_packages
import sys

# From https://github.com/pytest-dev/pytest-runner
needs_pytest = {'pytest', 'test', 'ptr'}.intersection(sys.argv)
pytest_runner = ['pytest-runner'] if needs_pytest else []

setup(name='dimredu',
      version='0.1',
      setup_requires=[]+pytest_runner,
      tests_require=['pytest'],
      packages=find_packages(),
      )
