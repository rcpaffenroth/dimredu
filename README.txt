README
-----------

Dependencies:

Numba (Checkout https://numba.pydata.org/numba-doc/dev/user/installing.html)

Python version 3.5.2

To install the code locally you need to run:

python3 setup.py develop

(sudo may need to be used when running the above command)

To see if things are ok, test the code by running:

python3 ./dimredu/eRPCAviaADMMFast.py

You should see something like the following (though, note that the exact output
may vary)

starting to generate data
total number of entries:  320000
observed entries: 4000
starting solve

criterion1 is the constraint
criterion2 is the solution
iteration criterion1 epsilon1 criterion2 epsilon2 rho      mu
       10   2.83e-03 1.00e-05   4.87e-04 1.00e-04 1.24e+00 3.55e-01
       20   1.02e-03 1.00e-05   1.49e-04 1.00e-04 1.24e+00 4.41e-01
       30   4.00e-04 1.00e-05   1.12e-04 1.00e-04 1.24e+00 6.78e-01
       40   1.40e-04 1.00e-05   9.84e-05 1.00e-04 1.24e+00 1.99e+00
       50   4.37e-05 1.00e-05   1.73e-05 1.00e-04 1.24e+00 4.72e+00
       60   1.09e-05 1.00e-05   2.89e-05 1.00e-04 1.24e+00 1.38e+01
       62   9.48e-06 1.00e-05   6.91e-06 1.00e-04 1.24e+00 2.13e+01
50.0213721348


How to run complete test suite (not required for general use)
-------------------------------------------------------------

To run the complete test suite you can do

python setup.py test

This will require cvxpy to complete successfully.

To install cvxpy you need to do

conda install -c cvxgrp scs  (note this may not be needed since the next line may do it automatically)
conda install -c cvxgrp cvxpy

And perhaps also:

conda install libgcc
sudo apt-get install liblapack3

NOTE:
At the moment (8/3/2017) cvxpy does not seem to work correctly on Windows with the error

FATAL: Cannot solve SDPs with > 2x2 matrices without linked blas+lapack libraries
Install blas+lapack and re-compile SCS with blas+lapack libray locations
ERROR: initCone failure
Failure:could not initialize work

